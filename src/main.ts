import { AppModule } from './app.module';
import 'reflect-metadata';
import {NestFactory} from '@nestjs/core';


const PORT = +process.env.PORT || 4000;
const HOST = process.env.HOST || 'localhost';

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  app.enableCors();
  await app.listen(PORT, HOST);
  console.log(`Application is listening on port ${HOST}:${PORT}`);
};

bootstrap();
