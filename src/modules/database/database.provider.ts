import {createConnection} from 'typeorm';
import {StarterProjectServerConfig} from '../config/starter-project-server-config';
import {STARTER_PROJECT_CONFIG_TOKEN} from '../config/config.providers';

export const DbConnectionToken = 'DbConnectionToken';

export const databaseProviders = [
    {
        provide: DbConnectionToken,
        useFactory: async (config: StarterProjectServerConfig) => {
            return await createConnection({
                ...config.dbConfig,
                entities: [
                    __dirname + '/../**/*.entity{.ts,.js}',
                ],
            });
        },
        inject: [STARTER_PROJECT_CONFIG_TOKEN],
    },
];
