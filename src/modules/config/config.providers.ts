import {StarterProjectServerConfig} from './starter-project-server-config';
import * as fs from 'fs';

const STARTER_PROJECT_CONFIG_PATH = process.env.PROJECT_STARTER_CONFIG_PATH ? process.env.PROJECT_STARTER_CONFIG_PATH : process.cwd() + '/config.json';
export const STARTER_PROJECT_CONFIG_TOKEN = 'STARTER_PROJECT_CONFIG_TOKEN';

export async function getProjectStarterServerConfig(): Promise<StarterProjectServerConfig> {
    return new Promise<StarterProjectServerConfig>((resolve, reject) => {
        fs.readFile(STARTER_PROJECT_CONFIG_PATH, 'utf8', function (err: any | null, data: string) {
            if (err) { reject(err) }
            resolve(JSON.parse(data) as StarterProjectServerConfig);
        });
    });
}


export const configProviders = [
    {
        provide: STARTER_PROJECT_CONFIG_TOKEN,
        useFactory: async (myLogger: any) => { return await getProjectStarterServerConfig(); }
    },
];
