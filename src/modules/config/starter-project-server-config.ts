import {PostgresConnectionOptions} from 'typeorm/driver/postgres/PostgresConnectionOptions';

export interface StarterProjectServerConfig {
    dbConfig: PostgresConnectionOptions;
}



