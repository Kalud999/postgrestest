import {Inject, Injectable} from "@nestjs/common";
import {Repository} from "typeorm";
import {Article} from "./article.entity";
import {StarterProjectServerConfig} from "../config/starter-project-server-config";
import {STARTER_PROJECT_CONFIG_TOKEN} from "../config/config.providers";
import {articleRepositoryToken} from "./article.providers";


@Injectable()
//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
export class ArticleService {

    constructor(@Inject(articleRepositoryToken) private readonly articleRepository: Repository<Article>,
                @Inject(STARTER_PROJECT_CONFIG_TOKEN) private config: StarterProjectServerConfig) {}


    async createArticle(): Promise<Article> {
        const article = new Article();
        article.name = 'Corona Summer';
        article.content = 'masked';
        article.author = 'God';
        return await this.articleRepository.save(article as Article);
    }


    async getAllArticles() {
         return await this.articleRepository.createQueryBuilder('article').getMany()
    }


}
//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°


