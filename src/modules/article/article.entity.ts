import {Entity, Column, PrimaryGeneratedColumn, BaseEntity} from 'typeorm';


@Entity()
export class Article extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    content: string;

    @Column()
    author: string;
}
