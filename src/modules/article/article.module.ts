import { Module } from '@nestjs/common';
import { ArticleController } from './article.controller';
import {DatabaseModule} from "../database/database.module";
import {ConfigModule} from "../config/config.module";
import {ArticleService} from "./article.service";
import {ArticleProviders} from "./article.providers";


@Module({
    imports : [
        DatabaseModule,
        ConfigModule,
    ],
    providers: [
        ArticleService,
        ...ArticleProviders,
    ],
    controllers: [ArticleController],
})
export class ArticleModule {}
