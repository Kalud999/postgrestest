import {Connection} from 'typeorm';
import {DbConnectionToken} from '../database/database.provider';
import {Article} from "./article.entity";

export const articleRepositoryToken = 'ArticleRepositoryToken';
export const ArticleProviders = [
    {
        provide: articleRepositoryToken,
        useFactory: (connection: Connection) => connection.getRepository(Article),
        inject: [DbConnectionToken],
    },
];

