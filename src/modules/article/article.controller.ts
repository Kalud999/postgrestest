import {Controller, Get, Inject, Post} from '@nestjs/common';
import {STARTER_PROJECT_CONFIG_TOKEN} from "../config/config.providers";
import {StarterProjectServerConfig} from "../config/starter-project-server-config";
import {ArticleService} from "./article.service";


@Controller('article')
export class ArticleController {
    constructor(@Inject(STARTER_PROJECT_CONFIG_TOKEN) private readonly config: StarterProjectServerConfig,
                private articleService: ArticleService
    ) {}

    @Post()
    createArticle() {
        return  this.articleService.createArticle();
    }


    @Get()
    getArticles() {
        return this.articleService.getAllArticles();
    }


} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
