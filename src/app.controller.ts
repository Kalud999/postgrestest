import { Controller, Get, Inject } from '@nestjs/common';


@Controller('test')
export class AppController {

    @Get()
    async test() {
        return { data : 'the backend works ;)' }
    }

}
