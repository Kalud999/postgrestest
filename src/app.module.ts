import { Module } from '@nestjs/common';
import {ConfigModule} from "./modules/config/config.module";
import {DatabaseModule} from "./modules/database/database.module";
import {ArticleModule} from "./modules/article/article.module";
import { AppController } from './app.controller';

@Module({
  imports: [
      ConfigModule,
      DatabaseModule,
      ArticleModule
  ],
    controllers: [AppController]
})

export class AppModule {}
